import { Connection, Result } from "db-conn";
import { Dialect } from "metal-dialect";

export class SqlAccess {
    dialect: Dialect;
    conn: Connection;
    constructor(conn:Connection, dialect: Dialect) {
        this.dialect = dialect;
        this.conn = conn;
    }
    public async findByKey(table:string, key:any):Promise<Result> {
        const sql = this.dialect.findByKeySql(table, key);
        const rt = await this.conn.execute(sql, key);
        return rt;
    }
    public async insert(table:string, data:any):Promise<void> {
        const sql = this.dialect.insertSql(table, data);
        const params: any[] = this.dialect.insertParams(table, data);
        const rt = await this.conn.execute(sql, params);
        if(rt.affectedRows!=1) {
            throw new Error("sql insert faled");
        }
    }
    public async update(table:string, key: any, data:any):Promise<void> {
        const sql = this.dialect.updateSql(table, key, data);
        const params: any[] = this.dialect.updateParams(table, key, data);
        const rt = await this.conn.execute(sql, params);
        if(rt.affectedRows!=1) {
            throw new Error("sql update faled");
        }
    }
    public async delete(table:string, key:any):Promise<void> {
        const sql = this.dialect.deleteSql(table, key);
        const rt = await this.conn.execute(sql, key);
        if(rt.affectedRows!=1) {
            throw new Error("sql delete faled");
        }
    }
    public async execute(sql:string, data:any):Promise<Result> {
        const rt = await this.conn.execute(sql, data);
        return rt;
    }
}


