
import { Connection, Result } from "db-conn";
import { AnsiDialect, ObjectMetadata } from "metal-dialect";
import { SqlAccess } from "../SqlAccess";

const mockConnection: any = {
	execute: jest.fn(),
	executeQuery: jest.fn()
}
const metadata = new ObjectMetadata();
const dialect = new AnsiDialect(metadata)
const oSqlAccess = new SqlAccess(mockConnection as Connection, dialect);

beforeEach(() => {
	mockConnection.execute.mockClear();
	mockConnection.executeQuery.mockClear();
});

test("insert", async () => {
	mockConnection.execute.mockResolvedValueOnce({affectedRows:1});
	const data = {
		a: 1,
		b: 2
	}
	await oSqlAccess.insert("TEST", data);
	expect(mockConnection.execute.mock.calls.length).toBe(1);
	expect(mockConnection.execute.mock.calls[0][0]).toStrictEqual('insert into "TEST"("a","b") values(?,?)');
	expect(mockConnection.execute.mock.calls[0][1]).toStrictEqual([1,2]);

});
test("insert fail", async () => {
	mockConnection.execute.mockResolvedValueOnce({affectedRows:0});
	const data = {
		a: 1,
		b: 2
	}
	try {
		await oSqlAccess.insert("TEST", data);
	}
	catch(err) {
		return
	}
	fail();
});
test("update", async () => {
	mockConnection.execute.mockResolvedValueOnce({affectedRows:1});
	const data = {
		a: 1,
		b: 2
	}
	await oSqlAccess.update("TEST", {Id:8}, data);
	expect(mockConnection.execute.mock.calls.length).toBe(1);
	expect(mockConnection.execute.mock.calls[0][0]).toStrictEqual('update "TEST" set "a"=?,"b"=? where "Id"=?');
	expect(mockConnection.execute.mock.calls[0][1]).toStrictEqual([1, 2, 8]);

});
test("update fail", async () => {
	mockConnection.execute.mockResolvedValueOnce({affectedRows:0});
	const data = {
		a: 1,
		b: 2
	}
	try {
		await oSqlAccess.update("TEST", 1, data);
	}
	catch(err) {
		return
	}
	fail();
});
test("delete", async () => {
	mockConnection.execute.mockResolvedValueOnce({affectedRows:1});
	await oSqlAccess.delete("TEST", {Id:1});
	expect(mockConnection.execute.mock.calls.length).toBe(1);
	expect(mockConnection.execute.mock.calls[0][0]).toStrictEqual('delete "TEST" where "Id"=:Id');
	expect(mockConnection.execute.mock.calls[0][1]).toStrictEqual({Id:1});
});

test("delete fail", async () => {
	mockConnection.execute.mockResolvedValueOnce({affectedRows:0});
	const data = {
		a: 1,
		b: 2
	}
	try {
		await oSqlAccess.delete("TEST", {Id:1});
	}
	catch(err) {
		return
	}
	fail();
});
test("find", async () => {
	const result: Result = {};
	mockConnection.execute.mockResolvedValueOnce(result);
	const rt = await oSqlAccess.findByKey("TEST", {Id:1});
	expect(mockConnection.execute.mock.calls.length).toBe(1);
	expect(mockConnection.execute.mock.calls[0][0]).toStrictEqual('select * from "TEST" where "Id"=:Id');
	expect(mockConnection.execute.mock.calls[0][1]).toStrictEqual({Id:1});
	expect(rt).toStrictEqual(result);
});

test("query", async () => {
	const result: Result = {};
	mockConnection.execute.mockResolvedValueOnce(result);
	const rt = await oSqlAccess.execute("select a,b,c from ABC", [1,2,3]);
	expect(mockConnection.execute.mock.calls.length).toBe(1);
	expect(mockConnection.execute.mock.calls[0][0]).toStrictEqual('select a,b,c from ABC');
	expect(mockConnection.execute.mock.calls[0][1]).toStrictEqual([1,2,3]);
	expect(rt).toStrictEqual(result);
});
